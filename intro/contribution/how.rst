.. _contribution-how:

How can I contribute?
#####################

The most obvious way to contribute is using the `OpenBeagle (BeagleBoard.org GitLab server) <https://openbeagle.org>`_ to report
bugs, suggest enhancements and providing merge requests, also called pull requests, the provide fixes to software, hardware
designs and documentation.

Reading the `help guide <https://openbeagle.org/help/>`_ is a great way to get started using OpenBeagle.

.. _contribution-todo-list:

Tackle to-do list
*****************

This documentation has a number of ``todo`` items where help is needed that can be searched in the source. This list will
show up directly in the staging documentation at https://docs.beagleboard.io/latest/intro/contribution/how.html#contribution-todo-list

.. todolist::

Google Summer of Code (GSoC)
****************************

For newcomers venturing into the realm of open-source contribution, Google Summer of Code (GSoC) stands as an invaluable platform. GSoC provides a unique opportunity to collaborate with the open-source community, engaging in the identification and development of exciting projects during the summer term.

BeagleBoard.org serves as a mentorship organization that takes part in the Google Summer of Code program actively, giving students the opportunity to work on open-source projects during the summer. Visit our dedicated :ref:`beagleboard-gsoc` for more information about this program, including past projects and mentorship opportunities. 

Reporting bugs
***************

Start by reading the `OpenBeagle Issues help page <https://openbeagle.org/help/user/project/issues/index.md>`_.

Please request an account and report any issues on the appropriate project issue tracker at https://openbeagle.org.

Report issues on the software images at https://openbeagle.org/explore/topics/distros.

Report issues on the hardware at https://openbeagle.org/explore/projects/topics/boards.

Suggesting enhancements
***********************

An issue doesn't have to be something wrong, it can just be about making something better. If in doubt how to make
a productive suggestion, hop on the forum and live chat groups to see what other people say. Check the current
ideas that are already out there and give us your idea. Try to be constructive in your suggestion. We are a primarily
a volunteer community looking to make your experience better, as those that follow you, and your suggestion could be
key in that endeavor.

Where available, use the "enhancement" `label <https://openbeagle.org/help/user/project/labels.md>`_ on your issue
to make sure we know you are looking for a future improvement, not reporting something critically wrong.

Submitting merge requests
*************************

If you want to contribute to a project, the most practical way is with a
`merge request <https://openbeagle.org/help/user/project/merge_requests/index.html>`_. Start
by `creating a fork <https://openbeagle.org/help/user/project/repository/forking_workflow.html>`_, which
is your own copy of the project you can feel free to edit how you see fit. When ready,
`create a merge request <https://openbeagle.org/help/user/project/merge_requests/creating_merge_requests.html>`_ and
we'll review your work and give comments back to you. If suitable, we'll update the code to include your contribution!

A bit more detailed suggestions can be found in the articles linked below.

Articles on contribution
**************************

- :ref:`beagleboard-git-usage`
- :ref:`beagleboard-doc-style`
- :ref:`rst-cheat-sheet`
- :ref:`beagleboard-linux-upstream`

History of contributors
***********************

- :ref:`contributors`
